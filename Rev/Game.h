#ifndef Game_h
#define Game_h

using namespace System;
using namespace System::Windows::Forms;

ref struct Tree
{
	array<array<int>^>^ m;
	array<array<int>^>^ r;
	array<Tree^>^ q;
	int n;
	Tree()
	{
		n = 0;
	}
};

ref class Game
{
public:
	Game();
	void Out(System::Drawing::Graphics^);
	void AI_Game();
	double AI_Game(array<array<int>^>^, Tree^, int);
	bool Play(int, int);
	void Play(int, int, array<array<int>^>^);
	bool CheckPlay(int, int);
	bool Check();
	void SetMove(int);
	void SetMove();
	int GetMove();
	int GetWin();
	static void EG(System::Drawing::Graphics^);
	static void About(System::Drawing::Graphics^);
	static void SetColors(System::Drawing::SolidBrush^, System::Drawing::SolidBrush^, System::Drawing::SolidBrush^, System::Drawing::SolidBrush^, System::Drawing::SolidBrush^, bool);
	static System::Drawing::SolidBrush^ cell;
	static System::Drawing::SolidBrush^ net;
	static System::Drawing::SolidBrush^ w;
	static System::Drawing::SolidBrush^ bl;
	static System::Drawing::SolidBrush^ point;
	static bool p, s = false;
private:
	array<array<int>^>^ f;
	Tree^ ai;
	int ai_c;
	static int win;
	static int win_w, win_b;
	int move;
	int a, b;
};

#endif