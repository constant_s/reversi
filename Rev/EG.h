#pragma once

#include "Game.h"

namespace Rev {

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

public ref class EG : public System::Windows::Forms::Form
{
public:
	EG(void)
	{
		InitializeComponent();
	}
protected:
	~EG()
	{
		if (components)
		{
			delete components;
		}
	}
private: System::Windows::Forms::PictureBox^  pictureBox1;
private: System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code

void InitializeComponent(void)
{
	this->pictureBox1 = (gcnew System::Windows::Forms::PictureBox());
	(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->BeginInit();
	this->SuspendLayout();
	// 
	// pictureBox1
	// 
	this->pictureBox1->BackColor = System::Drawing::Color::White;
	this->pictureBox1->Dock = System::Windows::Forms::DockStyle::Fill;
	this->pictureBox1->Location = System::Drawing::Point(0, 0);
	this->pictureBox1->Name = L"pictureBox1";
	this->pictureBox1->Size = System::Drawing::Size(525, 525);
	this->pictureBox1->TabIndex = 0;
	this->pictureBox1->TabStop = false;
	this->pictureBox1->Click += gcnew System::EventHandler(this, &EG::pictureBox1_Click);
	this->pictureBox1->Paint += gcnew System::Windows::Forms::PaintEventHandler(this, &EG::pictureBox1_Paint);
	// 
	// EG
	// 
	this->AutoScaleDimensions = System::Drawing::SizeF(8, 16);
	this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
	this->BackColor = System::Drawing::Color::White;
	this->ClientSize = System::Drawing::Size(525, 525);
	this->ControlBox = false;
	this->Controls->Add(this->pictureBox1);
	this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::Fixed3D;
	this->MaximizeBox = false;
	this->MinimizeBox = false;
	this->Name = L"EG";
	this->StartPosition = System::Windows::Forms::FormStartPosition::CenterParent;
	this->Text = L"���� ��������";
	(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->EndInit();
	this->ResumeLayout(false);
}

#pragma endregion

private: System::Void pictureBox1_Click(System::Object^  sender, System::EventArgs^  e) 
{
	this->Close();
}
private: System::Void pictureBox1_Paint(System::Object^  sender, System::Windows::Forms::PaintEventArgs^  e)
{
	Game::EG(e->Graphics);
}
};
}
