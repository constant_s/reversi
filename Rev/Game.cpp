#include "Game.h"

Game::Game()
{
	if (!s)
	{
		p = true;
		cell = gcnew System::Drawing::SolidBrush(System::Drawing::Color::Red);
		net = gcnew System::Drawing::SolidBrush(System::Drawing::Color::Black);
		w = gcnew System::Drawing::SolidBrush(System::Drawing::Color::White);
		bl = gcnew System::Drawing::SolidBrush(System::Drawing::Color::Black);
		point = gcnew System::Drawing::SolidBrush(System::Drawing::Color::Red);
	}
	a = b = 0;
	win = 3;
	ai = gcnew Tree;
	f = gcnew array<array<int>^>(8);
	for (int i = 0; i < 8; i++)
	{
		f[i] = gcnew array<int>(8);
		for (int j = 0; j < 8; j++)
			f[i][j] = 2;
	}
	f[3][3] = 1;
	f[3][4] = 0;
	f[4][3] = 0;
	f[4][4] = 1;
}

void Game::Out(System::Drawing::Graphics^ g)
{
	g->Clear(System::Drawing::Color::Red);
	for (int i = 0; i < 8; i++)
		for (int j = 0; j < 8; j++)
		{
		g->FillRectangle(net, j * 50, i * 50, 50, 50);
		g->FillRectangle(cell, j * 50 + 0.5, i * 50 + 0.5, 49, 49);
		if (f[i][j] == 1)
			g->FillEllipse(w, j * 50 + 0.5, i * 50 + 0.5, 48, 48);
		if (f[i][j] == 0)
			g->FillEllipse(bl, j * 50 + 0.5, i * 50 + 0.5, 48, 48);
		}
	if (p && move != 0)
		g->FillRectangle(System::Drawing::Brushes::Red, b * 50 + 22.5, a * 50 + 22.5, 5, 5);
}
bool Game::Play(int x, int y)
{
	int c = move % 2;
	bool p = false;
	int l, t;
	if (f[x][y] == 2)
	{
		t = 0;
		l = 0;
		for (int i = x + 1; i < 8; i++)
		{
			if (f[i][y] == 2)
				t++;
			if (f[i][y] == 1 - c && t == 0)
				l++;
			if (f[i][y] == c)
			{
				if (t == 0 && l != 0)
				{
					for (int j = i; j >= x; j--)
						f[j][y] = c;
					l = 0;
					p = true;
				}
				t++;
			}
		}
		t = 0;
		l = 0;
		for (int i = x - 1; i >= 0; i--)
		{
			if (f[i][y] == 2)
				t++;
			if (f[i][y] == 1 - c && t == 0)
				l++;
			if (f[i][y] == c)
			{
				if (t == 0 && l != 0)
				{
					for (int j = i; j <= x; j++)
						f[j][y] = c;
					l = 0;
					p = true;
				}
				t++;
			}
		}
		t = 0;
		l = 0;
		for (int i = y + 1; i < 8; i++)
		{
			if (f[x][i] == 2)
				t++;
			if (f[x][i] == 1 - c && t == 0)
				l++;
			if (f[x][i] == c)
			{
				if (t == 0 && l != 0)
				{
					for (int j = i; j >= y; j--)
						f[x][j] = c;
					l = 0;
					p = true;
				}
				t++;
			}
		}
		t = 0;
		l = 0;
		for (int i = y - 1; i >= 0; i--)
		{
			if (f[x][i] == 2)
				t++;
			if (f[x][i] == 1 - c && t == 0)
				l++;
			if (f[x][i] == c)
			{
				if (t == 0 && l != 0)
				{
					for (int j = i; j <= y; j++)
						f[x][j] = c;
					l = 0;
					p = true;
				}
				t++;
			}
		}
		t = 0;
		l = 0;
		for (int i = x + 1, j = y + 1; i < 8 && j < 8; i++, j++)
		{
			if (f[i][j] == 2)
				t++;
			if (f[i][j] == 1 - c && t == 0)
				l++;
			if (f[i][j] == c)
			{
				if (t == 0 && l != 0)
				{
					for (int k = i, h = j; k >= x && h >= y; k--, h--)
						f[k][h] = c;
					l = 0;
					p = true;
				}
				t++;
			}
		}
		t = 0;
		l = 0;
		for (int i = x + 1, j = y - 1; i < 8 && j >= 0; i++, j--)
		{
			if (f[i][j] == 2)
				t++;
			if (f[i][j] == 1 - c && t == 0)
				l++;
			if (f[i][j] == c)
			{
				if (t == 0 && l != 0)
				{
					for (int k = i, h = j; k >= x && h <= y; k--, h++)
						f[k][h] = c;
					l = 0;
					p = true;
				}
				t++;
			}
		}
		t = 0;
		l = 0;
		for (int i = x - 1, j = y + 1; i >= 0 && j < 8; i--, j++)
		{
			if (f[i][j] == 2)
				t++;
			if (f[i][j] == 1 - c && t == 0)
				l++;
			if (f[i][j] == c)
			{
				if (t == 0 && l != 0)
				{
					for (int k = i, h = j; k <= x && h >= y; k++, h--)
						f[k][h] = c;
					l = 0;
					p = true;
				}
				t++;
			}
		}
		t = 0;
		l = 0;
		for (int i = x - 1, j = y - 1; i >= 0 && j >= 0; i--, j--)
		{
			if (f[i][j] == 2)
				t++;
			if (f[i][j] == 1 - c && t == 0)
				l++;
			if (f[i][j] == c)
			{
				if (t == 0 && l != 0)
				{
					for (int k = i, h = j; k <= x && h <= y; k++, h++)
						f[k][h] = c;
					l = 0;
					p = true;
				}
				t++;
			}
		}
	}
	a = x;
	b = y;
	return p;
}
bool Game::CheckPlay(int x, int y)
{
	int c = move % 2;
	bool p = false;
	int l, t;
	if (f[x][y] == 2)
	{
		t = 0;
		l = 0;
		for (int i = x + 1; i < 8; i++)
		{
			if (f[i][y] == 2)
				t++;
			if (f[i][y] == 1 - c && t == 0)
				l++;
			if (f[i][y] == c)
			{
				if (t == 0 && l != 0)
				{
					l = 0;
					p = true;
				}
				t++;
			}
		}
		t = 0;
		l = 0;
		for (int i = x - 1; i >= 0; i--)
		{
			if (f[i][y] == 2)
				t++;
			if (f[i][y] == 1 - c && t == 0)
				l++;
			if (f[i][y] == c)
			{
				if (t == 0 && l != 0)
				{
					l = 0;
					p = true;
				}
				t++;
			}
		}
		t = 0;
		l = 0;
		for (int i = y + 1; i < 8; i++)
		{
			if (f[x][i] == 2)
				t++;
			if (f[x][i] == 1 - c && t == 0)
				l++;
			if (f[x][i] == c)
			{
				if (t == 0 && l != 0)
				{
					l = 0;
					p = true;
				}
				t++;
			}
		}
		t = 0;
		l = 0;
		for (int i = y - 1; i >= 0; i--)
		{
			if (f[x][i] == 2)
				t++;
			if (f[x][i] == 1 - c && t == 0)
				l++;
			if (f[x][i] == c)
			{
				if (t == 0 && l != 0)
				{
					l = 0;
					p = true;
				}
				t++;
			}
		}
		t = 0;
		l = 0;
		for (int i = x + 1, j = y + 1; i < 8 && j < 8; i++, j++)
		{
			if (f[i][j] == 2)
				t++;
			if (f[i][j] == 1 - c && t == 0)
				l++;
			if (f[i][j] == c)
			{
				if (t == 0 && l != 0)
				{
					l = 0;
					p = true;
				}
				t++;
			}
		}
		t = 0;
		l = 0;
		for (int i = x + 1, j = y - 1; i < 8 && j >= 0; i++, j--)
		{
			if (f[i][j] == 2)
				t++;
			if (f[i][j] == 1 - c && t == 0)
				l++;
			if (f[i][j] == c)
			{
				if (t == 0 && l != 0)
				{
					l = 0;
					p = true;
				}
				t++;
			}
		}
		t = 0;
		l = 0;
		for (int i = x - 1, j = y + 1; i >= 0 && j < 8; i--, j++)
		{
			if (f[i][j] == 2)
				t++;
			if (f[i][j] == 1 - c && t == 0)
				l++;
			if (f[i][j] == c)
			{
				if (t == 0 && l != 0)
				{
					l = 0;
					p = true;
				}
				t++;
			}
		}
		t = 0;
		l = 0;
		for (int i = x - 1, j = y - 1; i >= 0 && j >= 0; i--, j--)
		{
			if (f[i][j] == 2)
				t++;
			if (f[i][j] == 1 - c && t == 0)
				l++;
			if (f[i][j] == c)
			{
				if (t == 0 && l != 0)
				{
					l = 0;
					p = true;
				}
				t++;
			}
		}
	}
	return p;
}
bool Game::Check()
{
	int mas[2] = { 0 };
	for (int i = 0; i < 8; i++)
		for (int j = 0; j < 8; j++)
			if (f[i][j] < 2)
				mas[f[i][j]]++;
	win_b = mas[0];
	win_w = mas[1];
	if (mas[1] == 0)
	{
		win = 0;
		return true;
	}
	if (mas[0] == 0)
	{
		win = 1;
		return true;;
	}
	for (int i = 0; i < 8; i++)
		for (int j = 0; j < 8; j++)
			if (f[i][j] == 2)
				if (CheckPlay(i, j))
				{
					win = 3;
					return false;
				}
	SetMove();
	for (int i = 0; i < 8; i++)
		for (int j = 0; j < 8; j++)
			if (f[i][j] == 2)
				if (CheckPlay(i, j))
				{
					win = 3;
					MessageBox::Show("��� ����������� ����!");
					return false;
				}
	if (mas[0] == mas[1])
	{
		win = 2;
		return true;
	}
	if (mas[0] > mas[1])
		win = 0;
	else win = 1;
	return true;
}
void Game::SetMove(int i)
{
	move = i;
}
void Game::SetMove()
{
	move++;
}
int Game::GetMove()
{
	return move;
}
void Game::EG(System::Drawing::Graphics^ g)
{
	g->DrawString(win_b + " : " + win_w, gcnew System::Drawing::Font("FreeSerif", 24), System::Drawing::Brushes::Blue, 140, 200);
	if (win == 2)
	{
		g->DrawString("�����!", gcnew System::Drawing::Font("FreeSerif", 24), System::Drawing::Brushes::Blue, 135, 125);
	}
	else
		g->DrawString("�����������!", gcnew System::Drawing::Font("FreeSerif", 24), System::Drawing::Brushes::Blue, 90, 50);
	if (win == 0)
	{
		g->DrawString("������ �������� ������!", gcnew System::Drawing::Font("FreeSerif", 24), System::Drawing::Brushes::Blue, 20, 125);
	}
	if (win == 1)
	{
		g->DrawString("������ �������� �����!", gcnew System::Drawing::Font("FreeSerif", 24), System::Drawing::Brushes::Blue, 20, 125);
	}
	g->DrawString("������� �� ����!", gcnew System::Drawing::Font("FreeSerif", 24), System::Drawing::Brushes::Blue, 70, 275);
}
int Game::GetWin()
{
	return win;
}
void Game::AI_Game()
{
	ai_c = move % 2;
	int i;
	i = AI_Game(f, ai, 0);
	Play(ai->m[i][0], ai->m[i][1]);
}
double Game::AI_Game(array<array<int>^>^ p, Tree^ t, int o)
{
	double u[2] = { 0 };
	int s = GetMove();
	t->n = 0;
	o++;
	for (int i = 0; i < 8; i++)
		for (int j = 0; j < 8; j++)
			if (p[i][j] == 2)
				if (CheckPlay(i, j))
					t->n++;
	t->q = gcnew array<Tree^>(t->n);
	for (int i = 0; i < t->n; i++)
		t->q[i] = gcnew Tree;
	t->r = gcnew array<array<int>^>(8);
	for (int i = 0; i < 8; i++)
	{
		t->r[i] = gcnew array<int>(8);
		for (int j = 0; j < 8; j++)
			t->r[i][j] = p[i][j];
	}
	t->m = gcnew array<array<int>^>(t->n);
	for (int i = 0; i < t->n; i++)
		t->m[i] = gcnew array<int>(2);
	for (int k = 0; k < t->n;)
		for (int i = 0; i < 8; i++)
			for (int j = 0; j < 8; j++)
				if (p[i][j] == 2)
					if (CheckPlay(i, j))
					{
						t->m[k][0] = i;
						t->m[k][1] = j;
						k++;
					}
	if (o < 6)
		for (int i = 0; i < t->n; i++)
		{
		Play(t->m[i][0], t->m[i][1], t->r);
		if (int j = AI_Game(t->r, t->q[i], o) > u[0])
		{
			u[0] = j;
			u[1] = i;
		}
		else
			t->q[i] = nullptr;
		SetMove(s);
		for (int i = 0; i < 8; i++)
			for (int j = 0; j < 8; j++)
				t->r[i][j] = p[i][j];
		}
	o--;
	double mas[2] = { 0 };
	for (int i = 0; i < 8; i++)
		for (int j = 0; j < 8; j++)
			if (f[i][j] < 2)
				if ((i == 0 && j == 0) || (i == 7 && j == 0) || (i == 0 && j == 7) || (i == 7 && j == 7))
					mas[p[i][j]] += 3;
				else
					if (i == 0 || j == 0 || i == 7 || j == 7 || i == 2 || j == 2 || i == 5 || j == 5)
						mas[p[i][j]] += 2;
					else
						mas[p[i][j]]++;
	if (o > 0)
		if (ai_c == 0)
			return mas[0];
		else
			return mas[1];
	else
		return u[1];
}
void Game::Play(int x, int y, array<array<int>^>^ p)
{
	int c = move % 2;
	int l, t;
	if (p[x][y] == 2)
	{
		t = 0;
		l = 0;
		for (int i = x + 1; i < 8; i++)
		{
			if (p[i][y] == 2)
				t++;
			if (p[i][y] == 1 - c && t == 0)
				l++;
			if (p[i][y] == c)
			{
				if (t == 0 && l != 0)
				{
					for (int j = i; j >= x; j--)
						p[j][y] = c;
					l = 0;
				}
				t++;
			}
		}
		t = 0;
		l = 0;
		for (int i = x - 1; i >= 0; i--)
		{
			if (p[i][y] == 2)
				t++;
			if (p[i][y] == 1 - c && t == 0)
				l++;
			if (p[i][y] == c)
			{
				if (t == 0 && l != 0)
				{
					for (int j = i; j <= x; j++)
						p[j][y] = c;
					l = 0;
				}
				t++;
			}
		}
		t = 0;
		l = 0;
		for (int i = y + 1; i < 8; i++)
		{
			if (p[x][i] == 2)
				t++;
			if (p[x][i] == 1 - c && t == 0)
				l++;
			if (p[x][i] == c)
			{
				if (t == 0 && l != 0)
				{
					for (int j = i; j >= y; j--)
						p[x][j] = c;
					l = 0;
				}
				t++;
			}
		}
		t = 0;
		l = 0;
		for (int i = y - 1; i >= 0; i--)
		{
			if (p[x][i] == 2)
				t++;
			if (p[x][i] == 1 - c && t == 0)
				l++;
			if (p[x][i] == c)
			{
				if (t == 0 && l != 0)
				{
					for (int j = i; j <= y; j++)
						p[x][j] = c;
					l = 0;
				}
				t++;
			}
		}
		t = 0;
		l = 0;
		for (int i = x + 1, j = y + 1; i < 8 && j < 8; i++, j++)
		{
			if (p[i][j] == 2)
				t++;
			if (p[i][j] == 1 - c && t == 0)
				l++;
			if (p[i][j] == c)
			{
				if (t == 0 && l != 0)
				{
					for (int k = i, h = j; k >= x && h >= y; k--, h--)
						p[k][h] = c;
					l = 0;
				}
				t++;
			}
		}
		t = 0;
		l = 0;
		for (int i = x + 1, j = y - 1; i < 8 && j >= 0; i++, j--)
		{
			if (p[i][j] == 2)
				t++;
			if (p[i][j] == 1 - c && t == 0)
				l++;
			if (p[i][j] == c)
			{
				if (t == 0 && l != 0)
				{
					for (int k = i, h = j; k >= x && h <= y; k--, h++)
						p[k][h] = c;
					l = 0;
				}
				t++;
			}
		}
		t = 0;
		l = 0;
		for (int i = x - 1, j = y + 1; i >= 0 && j < 8; i--, j++)
		{
			if (p[i][j] == 2)
				t++;
			if (p[i][j] == 1 - c && t == 0)
				l++;
			if (p[i][j] == c)
			{
				if (t == 0 && l != 0)
				{
					for (int k = i, h = j; k <= x && h >= y; k++, h--)
						p[k][h] = c;
					l = 0;
				}
				t++;
			}
		}
		t = 0;
		l = 0;
		for (int i = x - 1, j = y - 1; i >= 0 && j >= 0; i--, j--)
		{
			if (p[i][j] == 2)
				t++;
			if (p[i][j] == 1 - c && t == 0)
				l++;
			if (p[i][j] == c)
			{
				if (t == 0 && l != 0)
				{
					for (int k = i, h = j; k <= x && h <= y; k++, h++)
						p[k][h] = c;
					l = 0;
				}
				t++;
			}
		}
	}
	a = x;
	b = y;
	SetMove();
}
void Game::About(System::Drawing::Graphics^ g)
{
	g->DrawString("R    E    V    E     R     S     I", gcnew System::Drawing::Font("FreeSerif", 24), System::Drawing::Brushes::Blue, 5, 125);
	g->DrawString("�����: �������� ����������", gcnew System::Drawing::Font("FreeSerif", 18), System::Drawing::Brushes::Blue, 60, 210);
	g->DrawString("  2015", gcnew System::Drawing::Font("FreeSerif", 16), System::Drawing::Brushes::Blue, 0, 350);
}
void Game::SetColors(System::Drawing::SolidBrush^a, System::Drawing::SolidBrush^b, System::Drawing::SolidBrush^c, System::Drawing::SolidBrush^d, System::Drawing::SolidBrush^e, bool f)
{
	p = f;
	cell = a;
	net = b;
	w = c;
	bl = d;
	point = e;
	s = true;
}