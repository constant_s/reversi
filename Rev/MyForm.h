#pragma once

#include "DW1.h"
#include "DW2.h"
#include "DW3.h"
#include "CD.h"
#include "EG.h"
#include "Game.h"

namespace Rev {

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

public ref class MyGame : public System::Windows::Forms::Form
{
private: int g, p;
private: Game^ game;
private: System::Windows::Forms::PictureBox^  pictureBox1;
private: System::Windows::Forms::MenuStrip^  menuStrip1;
private: System::Windows::Forms::ToolStripMenuItem^  ������ToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^  ���������ToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^  �����ToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^  ���������ToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^  ������ToolStripMenuItem1;
private: System::Windows::Forms::ToolStripMenuItem^  �����ToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^  ����������ToolStripMenuItem;
private: System::Windows::Forms::ToolStripMenuItem^  ����������ToolStripMenuItem;
public:
	MyGame(void)
	{
		g = 0;
		p = 0;
		InitializeComponent();
	}
protected:
	~MyGame()
	{
		if (components)
		{
			delete components;
		}
	}
	private: System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code

void InitializeComponent(void)
{
	this->pictureBox1 = (gcnew System::Windows::Forms::PictureBox());
	this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
	this->������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
	this->������ToolStripMenuItem1 = (gcnew System::Windows::Forms::ToolStripMenuItem());
	this->�����ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
	this->���������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
	this->�����ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
	this->���������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
	this->����������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
	this->����������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
	(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->BeginInit();
	this->menuStrip1->SuspendLayout();
	this->SuspendLayout();
	// 
	// pictureBox1
	// 
	this->pictureBox1->BackColor = System::Drawing::Color::White;
	this->pictureBox1->Dock = System::Windows::Forms::DockStyle::Fill;
	this->pictureBox1->Location = System::Drawing::Point(0, 28);
	this->pictureBox1->Name = L"pictureBox1";
	this->pictureBox1->Size = System::Drawing::Size(532, 493);
	this->pictureBox1->TabIndex = 0;
	this->pictureBox1->TabStop = false;
	this->pictureBox1->Paint += gcnew System::Windows::Forms::PaintEventHandler(this, &MyGame::pictureBox1_Paint);
	this->pictureBox1->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &MyGame::pictureBox1_MouseDown);
	// 
	// menuStrip1
	// 
	this->menuStrip1->BackColor = System::Drawing::Color::White;
	this->menuStrip1->ImageScalingSize = System::Drawing::Size(20, 20);
	this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(4) 
	{
		this->������ToolStripMenuItem,
		this->���������ToolStripMenuItem, this->����������ToolStripMenuItem, this->����������ToolStripMenuItem
	});
	this->menuStrip1->Location = System::Drawing::Point(0, 0);
	this->menuStrip1->Name = L"menuStrip1";
	this->menuStrip1->Size = System::Drawing::Size(532, 28);
	this->menuStrip1->TabIndex = 1;
	this->menuStrip1->Text = L"menuStrip1";
	// 
	// ������ToolStripMenuItem
	// 
	this->������ToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2)
	{
		this->������ToolStripMenuItem1,
		this->�����ToolStripMenuItem
	});
	this->������ToolStripMenuItem->Name = L"������ToolStripMenuItem";
	this->������ToolStripMenuItem->Size = System::Drawing::Size(55, 24);
	this->������ToolStripMenuItem->Text = L"����";
	// 
	// ������ToolStripMenuItem1
	// 
	this->������ToolStripMenuItem1->BackColor = System::Drawing::Color::White;
	this->������ToolStripMenuItem1->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Text;
	this->������ToolStripMenuItem1->Name = L"������ToolStripMenuItem1";
	this->������ToolStripMenuItem1->Size = System::Drawing::Size(175, 24);
	this->������ToolStripMenuItem1->Text = L"������";
	this->������ToolStripMenuItem1->Click += gcnew System::EventHandler(this, &MyGame::������ToolStripMenuItem1_Click);
	// 
	// �����ToolStripMenuItem
	// 
	this->�����ToolStripMenuItem->BackColor = System::Drawing::Color::White;
	this->�����ToolStripMenuItem->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Text;
	this->�����ToolStripMenuItem->Name = L"�����ToolStripMenuItem";
	this->�����ToolStripMenuItem->Size = System::Drawing::Size(175, 24);
	this->�����ToolStripMenuItem->Text = L"�����";
	this->�����ToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyGame::�����ToolStripMenuItem_Click);
	// 
	// ���������ToolStripMenuItem
	// 
	this->���������ToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) 
	{
		this->�����ToolStripMenuItem,
		this->���������ToolStripMenuItem
	});
	this->���������ToolStripMenuItem->Name = L"���������ToolStripMenuItem";
	this->���������ToolStripMenuItem->Size = System::Drawing::Size(98, 24);
	this->���������ToolStripMenuItem->Text = L"���������";
	// 
	// �����ToolStripMenuItem
	// 
	this->�����ToolStripMenuItem->BackColor = System::Drawing::Color::White;
	this->�����ToolStripMenuItem->Checked = true;
	this->�����ToolStripMenuItem->CheckOnClick = true;
	this->�����ToolStripMenuItem->CheckState = System::Windows::Forms::CheckState::Checked;
	this->�����ToolStripMenuItem->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Text;
	this->�����ToolStripMenuItem->Name = L"�����ToolStripMenuItem";
	this->�����ToolStripMenuItem->Size = System::Drawing::Size(175, 26);
	this->�����ToolStripMenuItem->Text = L"�����";
	this->�����ToolStripMenuItem->CheckStateChanged += gcnew System::EventHandler(this, &MyGame::�����ToolStripMenuItem_CheckStateChanged);
	// 
	// ���������ToolStripMenuItem
	// 
	this->���������ToolStripMenuItem->BackColor = System::Drawing::Color::White;
	this->���������ToolStripMenuItem->CheckOnClick = true;
	this->���������ToolStripMenuItem->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Text;
	this->���������ToolStripMenuItem->Name = L"���������ToolStripMenuItem";
	this->���������ToolStripMenuItem->Size = System::Drawing::Size(175, 26);
	this->���������ToolStripMenuItem->Text = L"���������";
	this->���������ToolStripMenuItem->CheckStateChanged += gcnew System::EventHandler(this, &MyGame::���������ToolStripMenuItem_CheckStateChanged);
	// 
	// ����������ToolStripMenuItem
	// 
	this->����������ToolStripMenuItem->Name = L"����������ToolStripMenuItem";
	this->����������ToolStripMenuItem->Size = System::Drawing::Size(113, 24);
	this->����������ToolStripMenuItem->Text = L"����������";
	this->����������ToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyGame::����������ToolStripMenuItem_Click);
	// 
	// ����������ToolStripMenuItem
	// 
	this->����������ToolStripMenuItem->Name = L"����������ToolStripMenuItem";
	this->����������ToolStripMenuItem->Size = System::Drawing::Size(118, 24);
	this->����������ToolStripMenuItem->Text = L"� ���������";
	this->����������ToolStripMenuItem->Click += gcnew System::EventHandler(this, &MyGame::����������ToolStripMenuItem_Click);
	// 
	// MyGame
	// 
	this->AutoScaleDimensions = System::Drawing::SizeF(8, 16);
	this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
	this->BackColor = System::Drawing::Color::White;
	this->ClientSize = System::Drawing::Size(532, 521);
	this->Controls->Add(this->pictureBox1);
	this->Controls->Add(this->menuStrip1);
	this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::Fixed3D;
	this->MainMenuStrip = this->menuStrip1;
	this->MaximizeBox = false;
	this->MinimizeBox = false;
	this->Name = L"MyGame";
	this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
	this->Text = L"R E V E R S I";
	(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->EndInit();
	this->menuStrip1->ResumeLayout(false);
	this->menuStrip1->PerformLayout();
	this->ResumeLayout(false);
	this->PerformLayout();
}

#pragma endregion

private: System::Void pictureBox1_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e)
{
	if (g != 0 && game->Play(e->Location.Y / 50, e->Location.X / 50))
	{
		if (p == 0 || game->GetMove() % 2 != p % 2)
		{
			if (p != 0)
				game->Out(pictureBox1->CreateGraphics());		
			pictureBox1->Invalidate();
			game->SetMove();
			if (game->Check())
			{
				pictureBox1->Invalidate();
				Rev::EG^ eg = gcnew Rev::EG;
				eg->ShowDialog();
				g = 0;
			}
		}
		if (p != 0 && g != 0)
		while (game->GetMove() % 2 == p % 2)
			{
				game->AI_Game();
				game->SetMove();
				pictureBox1->Invalidate();
				if (game->Check())
				{
					pictureBox1->Invalidate();
					game->SetMove();
					Rev::EG^ eg = gcnew Rev::EG;
					eg->ShowDialog();
					g = 0;
				}
			}
	}
}
private: System::Void �����ToolStripMenuItem_CheckStateChanged(System::Object^  sender, System::EventArgs^  e) 
{
	if (�����ToolStripMenuItem->Checked == true)
	{
		���������ToolStripMenuItem->Checked = false;
		p = 0;
	}
	else
	{
		���������ToolStripMenuItem->Checked = true;
		if (g == 0)
			p = 1;
		else
			p = game->GetMove() + 1;
	}
}
private: System::Void ���������ToolStripMenuItem_CheckStateChanged(System::Object^  sender, System::EventArgs^  e) 
{
	if (���������ToolStripMenuItem->Checked == true)
	{
		�����ToolStripMenuItem->Checked = false;
		if (g == 0)
			p = 1;
		else
			p = game->GetMove() + 1;
	}
	else
	{
		�����ToolStripMenuItem->Checked = true;
		p = 0;
	}
}
private: System::Void pictureBox1_Paint(System::Object^  sender, System::Windows::Forms::PaintEventArgs^  e) 
{
	if (g != 0)
	{
		game->Out(e->Graphics);
	}
	else
		Game::About(e->Graphics);
}
private: System::Void �����ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) 
{
	if (g != 0)
	{
		Rev::DW2^ d = gcnew Rev::DW2();
		if (d->ShowDialog() == System::Windows::Forms::DialogResult::OK)
			Application::Exit();
	}
	else Application::Exit();
}
private: System::Void ������ToolStripMenuItem1_Click(System::Object^  sender, System::EventArgs^  e) 
{
	if (g == 0)
	{
		game = gcnew Game;
		g++;
		game->SetMove(0);
		pictureBox1->Invalidate();

	}
	else
	{
		Rev::DW1^ d = gcnew Rev::DW1();
		if (d->ShowDialog() == System::Windows::Forms::DialogResult::OK)
		{
			game = gcnew Game;
			g++;
			game->SetMove(0);
			pictureBox1->Invalidate();
		}
	}
}
private: System::Void ����������ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) 
{
	Rev::CD^ d = gcnew Rev::CD();
	if (d->ShowDialog() == System::Windows::Forms::DialogResult::OK)
		pictureBox1->Invalidate();
}
private: System::Void ����������ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) 
{
	Rev::DW3^ d = gcnew Rev::DW3();
	d->ShowDialog();
}
};
}
