#pragma once

#include "Game.h"

namespace Rev {

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

public ref class CD : public System::Windows::Forms::Form
{
public:
	CD(void)
	{
		if (!Game::s)
		{
			p = true;
			cell = gcnew SolidBrush(Color::Red);
			net = gcnew SolidBrush(Color::Black);
			w = gcnew SolidBrush(Color::White);
			b = gcnew SolidBrush(Color::Black);
			point = gcnew SolidBrush(Color::Red);
		}
		else
		{
			p = Game::p;
			cell = Game::cell;
			net = Game::net;
			w = Game::w;
			b = Game::bl;
			point = Game::point;
		}
		InitializeComponent();
	}
protected:
	~CD()
	{
		if (components)
		{
			delete components;
		}
	}
private: System::Windows::Forms::Label^  label5;
private: System::Windows::Forms::Button^  button8;
private: System::Windows::Forms::Button^  button7;
private: System::Windows::Forms::Label^  label4;
private: System::Windows::Forms::Label^  label3;
private: System::Windows::Forms::Label^  label2;
private: System::Windows::Forms::Label^  label1;
private: System::Windows::Forms::PictureBox^  pictureBox1;
private: System::Windows::Forms::Button^  button6;
private: System::Windows::Forms::Button^  button5;
private: System::Windows::Forms::Button^  button4;
private: System::Windows::Forms::Button^  button3;
private: System::Windows::Forms::Button^  button2;
private: System::Windows::Forms::Button^  button1;
private: System::Windows::Forms::ColorDialog^  colorDialog1;
private: System::Windows::Forms::ColorDialog^  colorDialog2;
private: System::Windows::Forms::ColorDialog^  colorDialog3;
private: System::Windows::Forms::ColorDialog^  colorDialog4;
private: System::Windows::Forms::ColorDialog^  colorDialog5;
private: System::Windows::Forms::CheckBox^  checkBox1;
private: SolidBrush^ cell;
private: SolidBrush^ net;
private: SolidBrush^ w;
private: SolidBrush^ b;
private: SolidBrush^ point;
private: bool p;
private: System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code

void InitializeComponent(void)
	{
	this->label5 = (gcnew System::Windows::Forms::Label());
	this->button8 = (gcnew System::Windows::Forms::Button());
	this->button7 = (gcnew System::Windows::Forms::Button());
	this->label4 = (gcnew System::Windows::Forms::Label());
	this->label3 = (gcnew System::Windows::Forms::Label());
	this->label2 = (gcnew System::Windows::Forms::Label());
	this->label1 = (gcnew System::Windows::Forms::Label());
	this->pictureBox1 = (gcnew System::Windows::Forms::PictureBox());
	this->button6 = (gcnew System::Windows::Forms::Button());
	this->button5 = (gcnew System::Windows::Forms::Button());
	this->button4 = (gcnew System::Windows::Forms::Button());
	this->button3 = (gcnew System::Windows::Forms::Button());
	this->button2 = (gcnew System::Windows::Forms::Button());
	this->button1 = (gcnew System::Windows::Forms::Button());
	this->colorDialog1 = (gcnew System::Windows::Forms::ColorDialog());
	this->colorDialog2 = (gcnew System::Windows::Forms::ColorDialog());
	this->colorDialog3 = (gcnew System::Windows::Forms::ColorDialog());
	this->colorDialog4 = (gcnew System::Windows::Forms::ColorDialog());
	this->colorDialog5 = (gcnew System::Windows::Forms::ColorDialog());
	this->checkBox1 = (gcnew System::Windows::Forms::CheckBox());
	(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->BeginInit();
	this->SuspendLayout();
	// 
	// label5
	// 
	this->label5->AutoSize = true;
	this->label5->BackColor = System::Drawing::Color::White;
	this->label5->Font = (gcnew System::Drawing::Font(L"Segoe UI", 7.8F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
		static_cast<System::Byte>(204)));
	this->label5->Location = System::Drawing::Point(6, 326);
	this->label5->Name = L"label5";
	this->label5->Size = System::Drawing::Size(137, 19);
	this->label5->TabIndex = 27;
	this->label5->Text = L"���� ��������� ����";
	// 
	// button8
	// 
	this->button8->BackColor = System::Drawing::Color::White;
	this->button8->Font = (gcnew System::Drawing::Font(L"Segoe UI", 7.8F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
		static_cast<System::Byte>(204)));
	this->button8->Location = System::Drawing::Point(137, 386);
	this->button8->Name = L"button8";
	this->button8->Size = System::Drawing::Size(115, 37);
	this->button8->TabIndex = 26;
	this->button8->Text = L"��������";
	this->button8->UseVisualStyleBackColor = false;
	this->button8->Click += gcnew System::EventHandler(this, &CD::button8_Click);
	// 
	// button7
	// 
	this->button7->BackColor = System::Drawing::Color::White;
	this->button7->Font = (gcnew System::Drawing::Font(L"Segoe UI", 7.8F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
		static_cast<System::Byte>(204)));
	this->button7->Location = System::Drawing::Point(10, 386);
	this->button7->Name = L"button7";
	this->button7->Size = System::Drawing::Size(115, 37);
	this->button7->TabIndex = 25;
	this->button7->Text = L"���������";
	this->button7->UseVisualStyleBackColor = false;
	this->button7->Click += gcnew System::EventHandler(this, &CD::button7_Click);
	// 
	// label4
	// 
	this->label4->AutoSize = true;
	this->label4->BackColor = System::Drawing::Color::White;
	this->label4->Font = (gcnew System::Drawing::Font(L"Segoe UI", 7.8F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
		static_cast<System::Byte>(204)));
	this->label4->Location = System::Drawing::Point(6, 299);
	this->label4->Name = L"label4";
	this->label4->Size = System::Drawing::Size(138, 19);
	this->label4->TabIndex = 24;
	this->label4->Text = L"���� ������ �����";
	// 
	// label3
	// 
	this->label3->AutoSize = true;
	this->label3->BackColor = System::Drawing::Color::White;
	this->label3->Font = (gcnew System::Drawing::Font(L"Segoe UI", 7.8F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
		static_cast<System::Byte>(204)));
	this->label3->Location = System::Drawing::Point(6, 272);
	this->label3->Name = L"label3";
	this->label3->Size = System::Drawing::Size(129, 19);
	this->label3->TabIndex = 23;
	this->label3->Text = L"���� ����� �����";
	// 
	// label2
	// 
	this->label2->AutoSize = true;
	this->label2->BackColor = System::Drawing::Color::White;
	this->label2->Font = (gcnew System::Drawing::Font(L"Segoe UI", 7.8F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
		static_cast<System::Byte>(204)));
	this->label2->Location = System::Drawing::Point(6, 245);
	this->label2->Name = L"label2";
	this->label2->Size = System::Drawing::Size(77, 19);
	this->label2->TabIndex = 22;
	this->label2->Text = L"���� �����";
	// 
	// label1
	// 
	this->label1->AutoSize = true;
	this->label1->BackColor = System::Drawing::Color::White;
	this->label1->Font = (gcnew System::Drawing::Font(L"Segoe UI", 7.8F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
		static_cast<System::Byte>(204)));
	this->label1->Location = System::Drawing::Point(6, 218);
	this->label1->Name = L"label1";
	this->label1->Size = System::Drawing::Size(85, 19);
	this->label1->TabIndex = 21;
	this->label1->Text = L"���� ������";
	// 
	// pictureBox1
	// 
	this->pictureBox1->BackColor = System::Drawing::Color::White;
	this->pictureBox1->Location = System::Drawing::Point(33, 5);
	this->pictureBox1->Name = L"pictureBox1";
	this->pictureBox1->Size = System::Drawing::Size(200, 200);
	this->pictureBox1->TabIndex = 20;
	this->pictureBox1->TabStop = false;
	this->pictureBox1->Paint += gcnew System::Windows::Forms::PaintEventHandler(this, &CD::pictureBox1_Paint);
	// 
	// button6
	// 
	this->button6->BackColor = System::Drawing::Color::White;
	this->button6->Font = (gcnew System::Drawing::Font(L"Segoe UI", 7.8F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
		static_cast<System::Byte>(204)));
	this->button6->Location = System::Drawing::Point(10, 351);
	this->button6->Name = L"button6";
	this->button6->Size = System::Drawing::Size(242, 30);
	this->button6->TabIndex = 19;
	this->button6->Text = L"������� �������� �� ���������";
	this->button6->UseVisualStyleBackColor = false;
	this->button6->Click += gcnew System::EventHandler(this, &CD::button6_Click);
	// 
	// button5
	// 
	this->button5->BackColor = System::Drawing::Color::White;
	this->button5->Font = (gcnew System::Drawing::Font(L"Segoe UI", 7.8F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
		static_cast<System::Byte>(204)));
	this->button5->Location = System::Drawing::Point(186, 320);
	this->button5->Name = L"button5";
	this->button5->Size = System::Drawing::Size(66, 28);
	this->button5->TabIndex = 18;
	this->button5->Text = L"�������";
	this->button5->UseVisualStyleBackColor = false;
	this->button5->Click += gcnew System::EventHandler(this, &CD::button5_Click);
	// 
	// button4
	// 
	this->button4->BackColor = System::Drawing::Color::White;
	this->button4->Font = (gcnew System::Drawing::Font(L"Segoe UI", 7.8F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
		static_cast<System::Byte>(204)));
	this->button4->Location = System::Drawing::Point(186, 292);
	this->button4->Name = L"button4";
	this->button4->Size = System::Drawing::Size(66, 28);
	this->button4->TabIndex = 17;
	this->button4->Text = L"�������";
	this->button4->UseVisualStyleBackColor = false;
	this->button4->Click += gcnew System::EventHandler(this, &CD::button4_Click);
	// 
	// button3
	// 
	this->button3->BackColor = System::Drawing::Color::White;
	this->button3->Font = (gcnew System::Drawing::Font(L"Segoe UI", 7.8F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
		static_cast<System::Byte>(204)));
	this->button3->Location = System::Drawing::Point(186, 265);
	this->button3->Name = L"button3";
	this->button3->Size = System::Drawing::Size(66, 28);
	this->button3->TabIndex = 16;
	this->button3->Text = L"�������";
	this->button3->UseVisualStyleBackColor = false;
	this->button3->Click += gcnew System::EventHandler(this, &CD::button3_Click);
	// 
	// button2
	// 
	this->button2->BackColor = System::Drawing::Color::White;
	this->button2->Font = (gcnew System::Drawing::Font(L"Segoe UI", 7.8F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
		static_cast<System::Byte>(204)));
	this->button2->Location = System::Drawing::Point(186, 238);
	this->button2->Name = L"button2";
	this->button2->Size = System::Drawing::Size(66, 28);
	this->button2->TabIndex = 15;
	this->button2->Text = L"�������";
	this->button2->UseVisualStyleBackColor = false;
	this->button2->Click += gcnew System::EventHandler(this, &CD::button2_Click);
	// 
	// button1
	// 
	this->button1->BackColor = System::Drawing::Color::White;
	this->button1->Font = (gcnew System::Drawing::Font(L"Segoe UI", 7.8F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
		static_cast<System::Byte>(204)));
	this->button1->Location = System::Drawing::Point(186, 211);
	this->button1->Name = L"button1";
	this->button1->Size = System::Drawing::Size(66, 28);
	this->button1->TabIndex = 14;
	this->button1->Text = L"�������";
	this->button1->UseVisualStyleBackColor = false;
	this->button1->Click += gcnew System::EventHandler(this, &CD::button1_Click);
	// 
	// checkBox1
	// 
	this->checkBox1->AutoSize = true;
	this->checkBox1->BackColor = System::Drawing::Color::White;
	this->checkBox1->Checked = true;
	this->checkBox1->CheckState = System::Windows::Forms::CheckState::Checked;
	this->checkBox1->Location = System::Drawing::Point(162, 326);
	this->checkBox1->Name = L"checkBox1";
	this->checkBox1->Size = System::Drawing::Size(18, 17);
	this->checkBox1->TabIndex = 28;
	this->checkBox1->UseVisualStyleBackColor = false;
	this->checkBox1->CheckStateChanged += gcnew System::EventHandler(this, &CD::checkBox1_CheckStateChanged);
	// 
	// CD
	// 
	this->AutoScaleDimensions = System::Drawing::SizeF(7, 15);
	this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
	this->BackColor = System::Drawing::Color::White;
	this->ClientSize = System::Drawing::Size(260, 430);
	this->ControlBox = false;
	this->Controls->Add(this->checkBox1);
	this->Controls->Add(this->label5);
	this->Controls->Add(this->button8);
	this->Controls->Add(this->button7);
	this->Controls->Add(this->label4);
	this->Controls->Add(this->label3);
	this->Controls->Add(this->label2);
	this->Controls->Add(this->label1);
	this->Controls->Add(this->pictureBox1);
	this->Controls->Add(this->button6);
	this->Controls->Add(this->button5);
	this->Controls->Add(this->button4);
	this->Controls->Add(this->button3);
	this->Controls->Add(this->button2);
	this->Controls->Add(this->button1);
	this->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 7.2F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
		static_cast<System::Byte>(204)));
	this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::Fixed3D;
	this->Name = L"CD";
	this->StartPosition = System::Windows::Forms::FormStartPosition::CenterParent;
	this->Text = L"����������";
	(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->EndInit();
	this->ResumeLayout(false);
	this->PerformLayout();
}

#pragma endregion

private: System::Void pictureBox1_Paint(System::Object^  sender, System::Windows::Forms::PaintEventArgs^  e)
{
	System::Drawing::Graphics^ g = e->Graphics;
	for (int i = 0; i < 2; i++)
		for (int j = 0; j < 2; j++)
		{
			g->FillRectangle(net, j * 100, i * 100, 100, 100);
			g->FillRectangle(cell, j * 100 + 1, i * 100 + 1, 98, 98);
		}
	g->FillEllipse(b, 0, 0, 98, 98);
	g->FillEllipse(w, 0, 100, 98, 98);
	g->FillEllipse(w, 100, 0, 98, 98);
	g->FillEllipse(b, 100, 100, 98, 98);
	if (p)
		g->FillRectangle(point, 43, 43, 15, 15);
}
private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) 
{
	if (colorDialog1->ShowDialog() == System::Windows::Forms::DialogResult::OK)
		cell = gcnew SolidBrush(colorDialog1->Color);
	pictureBox1->Invalidate();
}
private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) 
{
	if (colorDialog2->ShowDialog() == System::Windows::Forms::DialogResult::OK)
		net = gcnew SolidBrush(colorDialog2->Color);
	pictureBox1->Invalidate();
}
private: System::Void button3_Click(System::Object^  sender, System::EventArgs^  e) 
{
	if (colorDialog3->ShowDialog() == System::Windows::Forms::DialogResult::OK)
		w = gcnew SolidBrush(colorDialog3->Color);
	pictureBox1->Invalidate();
}
private: System::Void button4_Click(System::Object^  sender, System::EventArgs^  e) 
{
	if (colorDialog4->ShowDialog() == System::Windows::Forms::DialogResult::OK)
		b = gcnew SolidBrush(colorDialog4->Color);
	pictureBox1->Invalidate();
}
private: System::Void button5_Click(System::Object^  sender, System::EventArgs^  e) 
{
	if (colorDialog5->ShowDialog() == System::Windows::Forms::DialogResult::OK)
		point = gcnew SolidBrush(colorDialog5->Color);
	pictureBox1->Invalidate();
}
private: System::Void checkBox1_CheckStateChanged(System::Object^  sender, System::EventArgs^  e) 
{
	p = checkBox1->Checked;
	pictureBox1->Invalidate();
}
private: System::Void button8_Click(System::Object^  sender, System::EventArgs^  e) 
{
	this->Close();
}
private: System::Void button7_Click(System::Object^  sender, System::EventArgs^  e) 
{
	Game::SetColors(cell, net, w, b, point, p);
	this->DialogResult = System::Windows::Forms::DialogResult::OK;
	this->Close();
}
private: System::Void button6_Click(System::Object^  sender, System::EventArgs^  e) 
{
	p = true;
	cell = gcnew SolidBrush(Color::Red);
	net = gcnew SolidBrush(Color::Black);
	w = gcnew SolidBrush(Color::White);
	b = gcnew SolidBrush(Color::Black);
	point = gcnew SolidBrush(Color::Red);
	pictureBox1->Invalidate();
}
};
}
